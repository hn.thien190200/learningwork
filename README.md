# Hướng Dẫn Deploy BE lên server
### Bước 1: Truy cập đến máy chủ ubuntu với tài khoản gadmin
### Bước 2: Chỉnh sửa tệp Host với câu lệnh : sudo nano /etc/hosts và thêm subdomain đã được cung cấp -> Lưu tệp và thoát.
![hosts](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694064888/deploy/z4671136319554_baf5f9b8ce3705ae3f7b9767dcc9b315_kycrqu.jpg)

### Bước 3: Cài đặt Nginx(bỏ qua nếu đã có), chạy lệnh "sudo apt install nginx" 
### Bước 4: Cấu hình NginX 
1. Tạo một tệp cấu hình mới cho Nginx proxy với lệnh **sudo nano /etc/nginx/sites-available/thienho.gcalls.vn**
2. Trong tệp cấu hình mới, thêm nội dung sau (đảm bảo thay đổi server_name, location, và proxy_pass phù hợp với cấu trúc dự án của bạn):
![cấu hình](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694065021/deploy/z4671231129494_e513bc91a92db10be1c78cbe1abf0f43_egrcvy.jpg)

3. Tạo liên kết từ tệp cấu hình đã tạo đến thư mục sites-enabled để kích hoạt cấu hình: **sudo ln -s /etc/nginx/sites-available/thienho.gcalls.vn /etc/nginx/sites-enabled/**
4. Kiểm tra cấu hình Nginx để đảm bảo không có lỗi với lệnh **sudo nginx -t** và khởi động lại nginx để áp dụng cấu hình mới với lệnh **sudo service nginx restart**
### Bước 5: Cài đặt Let's Encrypt và Đăng ký SSL cho subdomain :
1. Cài đặt Certbot (Let's Encrypt client) để lấy chứng chỉ SSL miễn phí cho subdomain với lệnh: **sudo apt install certbot**
2. Đăng ký chứng chỉ SSL cho subdomain (thay thế your_email@example.com bằng địa chỉ email của bạn và thienho.gcalls.vn bằng subdomain của bạn) với lệnh:
**sudo certbot certonly --nginx -d thienho.gcalls.vn -m your_email@example.com --agree-tos**
3. Certbot sẽ xác minh bạn là chủ sở hữu của domain bằng cách yêu cầu bạn thêm các thông tin cần thiết. Khi xong, Certbot sẽ lưu chứng chỉ SSL tại **/etc/letsencrypt/live/thienho.gcalls.vn/.**
4. Cấu hình Nginx để sử dụng chứng chỉ SSL vừa được cài đặt. Mở tệp cấu hình Nginx với lệnh : **sudo nano /etc/nginx/sites-available/thienho.gcalls.vn**
5. Sửa nội dung tệp cấu hình như sau (đảm bảo thay đổi server_name, ssl_certificate, và ssl_certificate_key phù hợp):
![Cấu hình2](https://res.cloudinary.com/dtlxfsgd5/image/upload/v1694064895/deploy/z4671215193400_c7b091d606df97b28ccf7a1595422cf9_dnu1c0.jpg)

6. Kiểm tra cấu hình Nginx: **sudo nginx -t**
7. Khởi động lại Nginx để áp dụng cấu hình mới: **sudo service nginx restart**
### Bước 6: clone source code BE lên server và triển khai
1. Cài đặt Node.js và npm (nếu chưa có) trên máy chủ: **sudo apt install nodejs npm** sau đó là cài đặt pm2 về : **npm install pm2**
2. Tạo một folder lưu trữ dự án và Clone dự án backend từ Git **(thay thế <git_repository_url>** bằng URL của kho lưu trữ Git của bạn) : **git clone <git_repository_url>**
3. Di chuyển vào thư mục dự án và chạy pm2 với tệp dự án vd: **pm2 start index.js**

